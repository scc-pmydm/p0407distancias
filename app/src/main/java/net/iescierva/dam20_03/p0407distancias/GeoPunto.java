package net.iescierva.dam20_03.p0407distancias;

//Por alguna razón no deja importar desde un módulo distinto a app
//Proxima vez probar con DataBinding
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import java.util.Objects;

public class GeoPunto implements Parcelable {

    private double longitud, latitud;

    /**
     * @see GeoPunto
     */
    public GeoPunto() {
        this.latitud = 0;
        this.longitud = 0;
    }

    /**
     * @param latitud  latitud en grados
     * @param longitud longitud en grados
     * @see GeoPunto
     */

    public GeoPunto(double latitud, double longitud) throws GeoException {
        set(latitud, longitud);
    }

    /**
     * @param latitud  latitud en millonésimas de grado
     * @param longitud longitud en millonésimas de grado
     * @see GeoPunto
     */

    public GeoPunto(int latitud, int longitud) throws GeoException {
        //los parámetros son millonésimas de grado
        this(latitud / 1e6, longitud / 1e6);
    }

    public GeoPunto(double latG, double latM, double latS,
                    double lonG, double lonM, double lonS) throws GeoException {
        //grados, minutos y segundos
        this(latG + latM / 60D + latS / 3600D,
                lonG + lonM / 60D + lonS / 3600D);
    }

    /**
     * @param punto punto geográfico
     * @return distancia (metros)
     * @see GeoPunto
     */
    public double distancia(GeoPunto punto) {
        final double RADIO_TIERRA = 6371000; // en metros
        double dLat = Math.toRadians(latitud - punto.latitud);
        double dLon = Math.toRadians(longitud - punto.longitud);
        double lat1 = Math.toRadians(punto.latitud);
        double lat2 = Math.toRadians(latitud);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                        Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return c * RADIO_TIERRA;
    }

    /**
     * @return longitud en grados
     * @see GeoPunto
     */
    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) throws GeoException {
        if (longitudOk(longitud))
            this.longitud = longitud;
        else
            throw new GeoException("GeoPunto.setLongitud(double): {" + longitud + "}");
    }

    /**
     * @return latitud en grados
     * @see GeoPunto
     */
    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) throws GeoException {
        if (latitudOk(latitud)) {
            this.latitud = latitud;
        } else
            throw new GeoException("GeoPunto.setlatitud(double): {" + latitud + "}");
    }

    /**
     * @param latitud  latitud en grados
     * @param longitud longitud en grados
     * @see GeoPunto
     */
    public void set(double latitud, double longitud) throws GeoException {
        setLatitud(latitud);
        setLongitud(longitud);
    }

    /**
     * @param latitud latitud en grados
     * @return true si es correcto
     * @see GeoPunto
     */
    private boolean latitudOk(double latitud) {
        return latitud >= -90. && latitud <= 90.;
    }

    /**
     * @param longitud longitud en grados
     * @return true si es correcto
     * @see GeoPunto
     */
    private boolean longitudOk(double longitud) {
        return longitud >= -180. && longitud <= 180.;
    }

    /**
     * @return punto en formato cadena
     * @see GeoPunto
     */
    @Override
    public String toString() {
        return "GeoPunto{" +
                "latitud=" + latitud +
                ",longitud=" + longitud +
                '}';
    }

    /**
     * @param o punto a comparar
     * @return true si es igual
     * @see GeoPunto
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeoPunto geoPunto = (GeoPunto) o;
        return Double.compare(geoPunto.longitud, longitud) == 0 &&
                Double.compare(geoPunto.latitud, latitud) == 0;
    }

    /**
     * @return hash de punto
     * @see GeoPunto
     */
    @Override
    public int hashCode() {
        return Objects.hash(latitud, longitud);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitud);
        dest.writeDouble(longitud);
    }

    private GeoPunto(Parcel in) {
        latitud = in.readDouble();
        longitud = in.readDouble();
    }

    public static final Parcelable.Creator<GeoPunto> CREATOR
            = new Parcelable.Creator<GeoPunto>() {
        public GeoPunto createFromParcel(Parcel in) {
            return new GeoPunto(in);
        }

        public GeoPunto[] newArray(int size) {
            return new GeoPunto[size];
        }
    };

    public static void mostrarZona (GeoPunto p1, GeoPunto p2, Activity a) throws GeoException {
        String t1 = p1.verZona();
        String t2 = p2.verZona();
        new AlertDialog.Builder(a)
                .setTitle("Zona:")
                .setMessage("p1 está en "+ t1 +" y p2 está en "+t2)
                .setPositiveButton("Aceptar", null)
                .show();
    }

    public String verZona()throws GeoException{
        String resultado = "Error";
        GeoPunto pNorte = new GeoPunto(90, this.getLongitud());
        GeoPunto pSur = new GeoPunto(-90, this.getLongitud());
        GeoPunto tCancer = new GeoPunto(23.43722, this.getLongitud());
        GeoPunto tCapricornio = new GeoPunto(-23.43722, this.getLongitud());
        GeoPunto cArtico = new GeoPunto(66.56278,this.getLongitud());
        GeoPunto cAntartico = new GeoPunto(-66.56278,this.getLongitud());
        GeoPunto ecuador = new GeoPunto(0,this.getLongitud());
        if (this.distancia(pNorte)==0)
            resultado =  "el polo norte";
        else if (this.distancia(pSur)==0)
            resultado =  "el polo sur";
        else if (this.distancia(tCancer)==0)
            resultado = "el trópico de Cáncer";
        else if (this.distancia(tCapricornio)==0)
            resultado = "el trópico de Capricornio";
        else if (this.distancia(cArtico)==0)
            resultado = "el círculo polar Ártico";
        else if(this.distancia(cAntartico)== 0)
            resultado = "el círculo polar Antártico";
        else if(this.distancia(ecuador)==0)
            resultado = "el ecuador";
        else if(this.getLatitud()< pNorte.getLatitud() && this.getLatitud()>cArtico.getLatitud())
            resultado = "la zona polar norte";
        else if(this.getLatitud()> pSur.getLatitud() && this.getLatitud()<cAntartico.getLatitud())
            resultado = "la zoina polar sur";
        else if(this.getLatitud()< tCancer.getLatitud()&&this.getLatitud()> tCapricornio.getLatitud())
            resultado = "la zona intertropical";
        else if(this.getLatitud()< cArtico.getLatitud() && this.getLatitud()> tCancer.getLatitud())
            resultado = "la zona inmediata norte;"
        else if(this.getLatitud()> cAntartico.getLatitud() &&this.getLatitud()< tCapricornio.getLatitud())
            resultado = "la zona inmediata sur";
        return resultado;

    }
}