package net.iescierva.dam20_03.p0407distancias;

import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    //Acceso a Punto 1
    EditText p1LatG, p1LatM, p1LatS;
    EditText p1LonG, p1LonM, p1LonS;
    ToggleButton p1LatMinus, p1LonMinus;
    //Acceso a Punto 2
    EditText p2LatG, p2LatM, p2LatS;
    EditText p2LonG, p2LonM, p2LonS;
    ToggleButton p2LatMinus, p2LonMinus;

    TextView distancia;

    Button btnCalcular, btnBorrar, btnZona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p1LatMinus = findViewById(R.id.p1LatMinus);
        p1LonMinus = findViewById(R.id.p1LonMinus);
        p1LatG = findViewById(R.id.p1LatG);
        p1LatM = findViewById(R.id.p1LatM);
        p1LatS = findViewById(R.id.p1LatS);
        p1LonG = findViewById(R.id.p1LonG);
        p1LonM = findViewById(R.id.p1LonM);
        p1LonS = findViewById(R.id.p1LonS);
        //Acceso a Punto 2
        p2LatMinus = findViewById(R.id.p2LatMinus);
        p2LonMinus = findViewById(R.id.p2LonMinus);
        p2LatG = findViewById(R.id.p2LatG);
        p2LatM = findViewById(R.id.p2LatM);
        p2LatS = findViewById(R.id.p2LatS);
        p2LonG = findViewById(R.id.p2LonG);
        p2LonM = findViewById(R.id.p2LonM);
        p2LonS = findViewById(R.id.p2LonS);

        distancia = findViewById(R.id.distancia);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnZona = findViewById(R.id.btnZona);

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1LatMinus.setChecked(false);
                p1LonMinus.setChecked(false);
                p1LatG.setText("");
                p1LatM.setText("");
                p1LatS.setText("");
                p1LonG.setText("");
                p1LonM.setText("");
                p1LonS.setText("");

                p2LatMinus.setChecked(false);
                p2LonMinus.setChecked(false);
                p2LatG.setText("");
                p2LatM.setText("");
                p2LatS.setText("");
                p2LonG.setText("");
                p2LonM.setText("");
                p2LonS.setText("");
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Hecho el checkeo de los polos
                    GeoPunto puntoPrueba = new GeoPunto(66, 33, 46, 0, 0, 0);
                    GeoPunto p1 = new GeoPunto(
                            p1LatG.getText().toString().isEmpty() ? 0 : Double.parseDouble(p1LatG.getText().toString()),
                            p1LatM.getText().toString().isEmpty() ? 0 : Double.parseDouble(p1LatM.getText().toString()),
                            p1LatS.getText().toString().isEmpty() ? 0 : Double.parseDouble(p1LatS.getText().toString()),
                            p1LonG.getText().toString().isEmpty() ? 0 : Double.parseDouble(p1LonG.getText().toString()),
                            p1LonM.getText().toString().isEmpty() ? 0 : Double.parseDouble(p1LonM.getText().toString()),
                            p1LonS.getText().toString().isEmpty() ? 0 : Double.parseDouble(p1LonS.getText().toString())
                    );
                    if (p1LatMinus.isChecked()) p1.setLatitud((-1) * p1.getLatitud());
                    if (p1LonMinus.isChecked()) p1.setLongitud((-1) * p1.getLongitud());

                    GeoPunto p2 = new GeoPunto(
                            p2LatG.getText().toString().isEmpty() ? 0 : Double.parseDouble(p2LatG.getText().toString()),
                            p2LatM.getText().toString().isEmpty() ? 0 : Double.parseDouble(p2LatM.getText().toString()),
                            p2LatS.getText().toString().isEmpty() ? 0 : Double.parseDouble(p2LatS.getText().toString()),
                            p2LonG.getText().toString().isEmpty() ? 0 : Double.parseDouble(p2LonG.getText().toString()),
                            p2LonM.getText().toString().isEmpty() ? 0 : Double.parseDouble(p2LonM.getText().toString()),
                            p2LonS.getText().toString().isEmpty() ? 0 : Double.parseDouble(p2LonS.getText().toString())
                    );
                    if (p2LatMinus.isChecked()) p2.setLatitud((-1) * p2.getLatitud());
                    if (p2LonMinus.isChecked()) p2.setLongitud((-1) * p2.getLongitud());

                    if(Math.abs(p2.getLatitud())>=puntoPrueba.getLatitud()||Math.abs(p1.getLatitud())>=puntoPrueba.getLatitud()){
                        distancia.setText("ERROR: Demasiado cerca de los polos");
                    }else {
                        double result = p1.distancia(p2) / 1000;
                        Locale l = Locale.getDefault();
                        distancia.setText(String.format(l, "%10.2f", result) + " km");
                    }
                } catch (Exception e) {
                   /* ejemplo de flag de errores
                   if (p1LatG.getText().toString().matches(""))
                          p1LatG.setError(getResources().getString(R.string.error_valor_incorrecto));
                   */
                    distancia.setText(getResources().getString(R.string.error_generico));
                }
            }
        });

        btnZona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    GeoPunto p1 = new GeoPunto(Double.parseDouble(p1LatG.getText().toString()), Double.parseDouble(p1LatM.getText().toString()),
                            Double.parseDouble(p1LatS.getText().toString()),Double.parseDouble(p1LonG.getText().toString()),
                            Double.parseDouble(p1LonM.getText().toString()), Double.parseDouble(p1LonS.getText().toString()));
                    GeoPunto p2 = new GeoPunto(Double.parseDouble(p2LatG.getText().toString()), Double.parseDouble(p2LatM.getText().toString()),
                            Double.parseDouble(p2LatS.getText().toString()),Double.parseDouble(p2LonG.getText().toString()),
                            Double.parseDouble(p2LonM.getText().toString()), Double.parseDouble(p2LonS.getText().toString()));
                    MainActivity.mostrarZona(p1, p2);
                } catch (GeoException e) {
                    e.printStackTrace();
                }
            }
        });


    }
    public static void mostrarZona(GeoPunto p1, GeoPunto p2){
        GeoPunto.mostrarZona(p1,p2, this);
    }
}